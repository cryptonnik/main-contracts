pragma solidity ^0.5.0;

import "./BaseContract.sol";

contract ManagableBaseContract is BaseContract {

	event ManagerChanged(address oldManager, address newManager);

	address public manager;

	modifier onlyManager() {
		require(msg.sender == manager);
		_;
	}

	function setManager(address _newManager) public onlyAdmin {

		emit ManagerChanged(manager, _newManager);
		manager = _newManager;
	}
}
