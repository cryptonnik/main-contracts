pragma solidity ^0.5.0;

import "../common/ManagableBaseContract.sol";
import "../utils/AddressHashSet.sol";
import "../openzeppelin/ERC20.sol";


interface IToken {

	function getAccountBalances(address[] calldata accounts) external view
			returns (uint tokenSum, uint[] memory balances);
}

contract VotingOld is ManagableBaseContract {

	using AddressHashSet for AddressHashSet.Data;
	using SafeMath for uint256;

	event SubjectCreated(uint id, uint8 optionCount, uint choiceLimit);
	event SubjectRemoved(uint id);
	event SubjectVoted(uint id);

	uint8 constant TYPE_SINGLE_CHOICE = 0;
	uint8 constant TYPE_MULTIPLE_CHOICE = 1;

	uint8 constant STATUS_NONE = 0;
	uint8 constant STATUS_ACTIVE = 1;
	uint8 constant STATUS_FINISHED = 2;

	struct Subject {

		uint8 status;
		uint8 optionCount;
		uint8 choiceLimit;

		uint tokensCanVote;
		mapping(address => uint) voterBalances;
		mapping(uint8 => uint) optionPoints;
	}

	uint subjectIdCounter;
	mapping (uint => Subject) subjects;
	address tokenContractAddress;

	AddressHashSet.Data voterSet;
	AddressHashSet.Data evangelistSet;

	mapping(address => uint) reservedBalances;

	constructor() public {
		performStateUpgrade();
	}

	function performStateUpgrade() internal {
		admin = msg.sender;
		manager = msg.sender;
	}

	function version() public view returns (string memory) {
		return "0.1.0";
	}

	// ==========================================

	function addVoter(address _voter) public onlyManager {
		voterSet.add(_voter);
	}

	function removeVoter(address _voter) public onlyManager {
		voterSet.remove(_voter);
	}

	function addEvangelist(address _evengelist) public onlyManager {
		evangelistSet.add(_evengelist);
	}

	function removeEvangelist(address _evengelist) public onlyManager {
		evangelistSet.remove(_evengelist);
	}

	function setReservedBalance(address _voter, uint _balance) public onlyManager {
		reservedBalances[_voter] = _balance;
	}

	// ==========================================

	function createSubject(uint8 optionCount, uint8 choiceLimit) public onlyManager {

		subjectIdCounter++;

		Subject storage subject = subjects[subjectIdCounter];
		subject.status = STATUS_ACTIVE;
		subject.optionCount = optionCount;
		subject.choiceLimit = choiceLimit;

		IToken tokenContract = IToken(tokenContractAddress);

		(uint tokenSum, uint[] memory balances) = tokenContract.getAccountBalances(voterSet.elements);

		address voter;
		uint reservedBalance;

		for(uint i = 0; i < voterSet.elements.length; i++) {

			voter = voterSet.elements[i];
			reservedBalance = reservedBalances[voter];
			subject.voterBalances[voter] = balances[i];

			tokenSum = tokenSum.add(reservedBalance);
		}

		subject.tokensCanVote = tokenSum;

		emit SubjectCreated(subjectIdCounter, optionCount, choiceLimit);
	}

	function finishVoting(uint subjectId) public onlyManager {

		Subject storage subject = subjects[subjectId];

		require(subject.status == STATUS_ACTIVE);
		subject.status = STATUS_FINISHED;
	}

	function vote(uint subjectId, uint8 decision) public {

		Subject storage subject = subjects[subjectId];

		require(subject.status == STATUS_ACTIVE);

		uint votingPoints;

		uint maxPoints = subject.tokensCanVote / 100;

		if(evangelistSet.contains(msg.sender)) {

			votingPoints = maxPoints;

		} else {

			votingPoints = subject.voterBalances[msg.sender];
			votingPoints = votingPoints > maxPoints ? maxPoints : votingPoints;
		}

		require(votingPoints > 0);

		uint choicesMade = 0;

		for(uint8 i = 0; i < subject.optionCount; i++) {

			bool optionChosen = ((0x01 << i) & decision) != 0x00;

			if(optionChosen) {

				choicesMade++;
				subject.optionPoints[i] = subject.optionPoints[i].add(votingPoints);

				if(choicesMade >= subject.choiceLimit) {

					break;
				}
			}
		}
	}

	function stateOfSubject(uint subjectId) public view
			returns (
					uint8 status,
					uint8 choiceLimit,
					uint[] memory optionPoints)
	{
		Subject storage subject = subjects[subjectId];

		status = subject.status;
		choiceLimit = subject.choiceLimit;

		optionPoints = new uint[](subject.optionCount);

		for(uint8 i = 0; i < subject.optionCount; i++) {

			optionPoints[i] = subject.optionPoints[i];
		}
	}
}
