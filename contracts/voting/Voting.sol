pragma solidity ^0.5.0;

import "../common/ManagableBaseContract.sol";
import "../utils/AddressHashSet.sol";
import "../openzeppelin/ERC20.sol";


interface IToken {

	function freezeAllTokens(address account, uint expirationTime) external returns (uint totalBalance);
}

contract Voting is ManagableBaseContract {

	using AddressHashSet for AddressHashSet.Data;
	using SafeMath for uint256;

	event SubjectCreated(uint id, uint8 optionCount, uint choiceLimit);
	event SubjectRemoved(uint id);
	event SubjectVoted(uint id);

	uint8 constant TYPE_SINGLE_CHOICE = 0;
	uint8 constant TYPE_MULTIPLE_CHOICE = 1;

	uint8 constant STATUS_NONE = 0;
	uint8 constant STATUS_ACTIVE = 1;		// TODO: Make it activated
	uint8 constant STATUS_FINISHED = 2;		// TODOL Create a removed state

	struct VoterDecision {

		uint8 decision;
		bool isEvangelist;
		uint frozenBalance;
	}

	struct Subject {

		uint8 status;
		uint8 optionCount;
		uint8 choiceLimit;
		uint expirationTime;

		address[] voters;
		mapping(address => VoterDecision) decisions;
	}

	uint subjectIdCounter;
	mapping (uint => Subject) subjects;
	address tokenContractAddress;

	AddressHashSet.Data voterSet;
	AddressHashSet.Data evangelistSet;

	constructor() public {
		performStateUpgrade();
	}

	function performStateUpgrade() internal {
		admin = msg.sender;
		manager = msg.sender;
	}

	function version() public view returns (string memory) {
		return "0.2.0";
	}

	// ==========================================

	function addVoter(address _voter) public onlyManager {
		voterSet.add(_voter);
	}

	function removeVoter(address _voter) public onlyManager {
		voterSet.remove(_voter);
	}

	function addEvangelist(address _evengelist) public onlyManager {
		evangelistSet.add(_evengelist);
	}

	function removeEvangelist(address _evengelist) public onlyManager {
		evangelistSet.remove(_evengelist);
	}

	// ==========================================

	function createSubject(uint8 optionCount, uint8 choiceLimit, uint expirationTime) public onlyManager {

		subjectIdCounter++;

		Subject storage subject = subjects[subjectIdCounter];
		subject.status = STATUS_ACTIVE;
		subject.optionCount = optionCount;
		subject.choiceLimit = choiceLimit;
		subject.expirationTime = expirationTime;

		emit SubjectCreated(subjectIdCounter, optionCount, choiceLimit);
	}

	function vote(uint subjectId, uint8 decision) public {

		Subject storage subject = subjects[subjectId];

		require(subject.status == STATUS_ACTIVE);
		require(subject.expirationTime > block.timestamp);

		bool isEvangelist = evangelistSet.contains(msg.sender);

		require(isEvangelist || voterSet.contains(msg.sender));

		subject.voters.push(msg.sender);

		IToken tokenContract = IToken(tokenContractAddress);

		uint voterBalance = tokenContract.freezeAllTokens(msg.sender, subject.expirationTime);

		VoterDecision storage voterDecision = subject.decisions[msg.sender];

		voterDecision.decision = decision;
		voterDecision.isEvangelist = isEvangelist;
		voterDecision.frozenBalance = voterBalance;
	}

	function calculateVotingResults(uint subjectId) public returns (uint[] memory optionResults) {

		Subject storage subject = subjects[subjectId];

		require(subject.status == STATUS_ACTIVE);

		uint optionCount = subject.optionCount;
		uint choiceLimit = subject.choiceLimit;

		optionResults = new uint[](optionCount);

		uint totalNumberOfTokens = 0;

		for(uint i = 0; i < subject.voters.length; i++) {

			address voterAddress = subject.voters[i];
			totalNumberOfTokens = totalNumberOfTokens.add(subject.decisions[voterAddress].frozenBalance);
		}

		uint votingTokensLimit = totalNumberOfTokens / 100;

		for(uint i = 0; i < subject.voters.length; i++) {

			address voterAddress = subject.voters[i];
			VoterDecision storage voterDecision = subject.decisions[voterAddress];

			uint votingTokens;

			if(voterDecision.isEvangelist) {

				votingTokens = votingTokensLimit;

			} else {

				votingTokens = voterDecision.frozenBalance;

				if(votingTokens > votingTokensLimit)
					votingTokens = votingTokensLimit;
			}

			addOptionPoints(
					optionCount,
					choiceLimit,
					voterDecision.decision,
					votingTokens,
					optionResults);
		}
	}

	function addOptionPoints(uint optionCount, uint choiceLimit, uint8 decision, uint votingTokens, uint[] memory optionResults) internal {

		uint choicesMade = 0;

		for(uint8 j = 0; j < optionCount; j++) {

			bool optionChosen = ((0x01 << j) & decision) != 0x00;

			if(optionChosen) {

				choicesMade++;
				optionResults[j] = optionResults[j].add(votingTokens);

				if(choicesMade >= choiceLimit) {

					break;
				}
			}
		}
	}

	/*
	function finishVoting(uint subjectId) public onlyManager {

		Subject storage subject = subjects[subjectId];

		require(subject.status == STATUS_ACTIVE);
		subject.status = STATUS_FINISHED;
	}
	*/

	/*
	function stateOfSubject(uint subjectId) public view
			returns (
					uint8 status,
					uint8 choiceLimit,
					uint[] optionPoints)
	{
		Subject storage subject = subjects[subjectId];

		status = subject.status;
		choiceLimit = subject.choiceLimit;

		optionPoints = new uint[](subject.optionCount);

		for(uint8 i = 0; i < subject.optionCount; i++) {

			optionPoints[i] = subject.optionPoints[i];
		}
	}
	*/
}
