pragma solidity ^0.5.0;

import "../openzeppelin/SafeMath.sol";
import "./AddressHashSet.sol";

contract MultisigContractCaller {

	using SafeMath for uint256;
	using AddressHashSet for AddressHashSet.Data;

	struct CandidateState {

		uint iterationNumber;
		uint confirmations;
		mapping(address => uint) signerLastIterationNumbers;
	}

	struct ActionFlowState {

		uint confirmationLevel;

		bytes32 currentCallHash;
		uint callNumber;
		uint callConfirmations;

		mapping(address => uint) signerLastCallNumbers;
	}

	uint constant ACTION_FLOW_ADMIN = 1;

	uint8 constant ITERATION_STAGES = 2;
	uint8 constant ITERATION_STAGE_ADDING_USER = 1;
	uint8 constant ITERATION_STAGE_REMOVING_USER = 0;

	mapping(address => CandidateState) candidateStates;
	AddressHashSet.Data signerSet;

	mapping(uint => ActionFlowState) actionFlowStates;
	mapping(bytes4 => uint) methodActionFlowIds;

	address public targetContractAddress;

	// ========================================

	constructor() public {

		signerSet.add(msg.sender);

		CandidateState storage state = candidateStates[msg.sender];
		state.iterationNumber = 2;
	}

	function __addSigner(address _signer) external {

		if(!processCall(ACTION_FLOW_ADMIN))
			return;

		setCandidateState(_signer, true);
	}

	function __removeSigner(address _signer) external {

		if(!processCall(ACTION_FLOW_ADMIN))
			return;

		setCandidateState(_signer, false);
	}

	function __setTargetContract(address _contractAddress) external {

		if(!processCall(ACTION_FLOW_ADMIN))
			return;

		targetContractAddress = _contractAddress;
	}

	function __setActionFlowConfirmationLevel(uint _actionFlowId, uint _confirmationLevel) external {

		if(!processCall(ACTION_FLOW_ADMIN))
			return;

		require(_confirmationLevel <= signerSet.elements.length);

		ActionFlowState storage state = actionFlowStates[_actionFlowId];
		state.confirmationLevel = _confirmationLevel;
	}

	function __setMethodActionFlow(string calldata _methodSignature, uint _actionFlowId) external {

		if(!processCall(ACTION_FLOW_ADMIN))
			return;

		//methodActionFlowIds[bytes4(keccak256(_methodSignature))] = _actionFlowId;
		methodActionFlowIds[convertBytesToBytes4(abi.encodeWithSignature("upgradeState()"))] = _actionFlowId;
	}

	function __getSigners() public view returns (address[] memory) {
		return signerSet.elements;
	}

/*
	uint iterationNumber;
	uint confirmations;
	mapping(address => uint) lastConfirmedIteration;
*/
	function __getCandidateState(address _signer) external view
			returns (bool isSigner, uint iterationNumber, uint confirmations)
	{

	}

	function __getsignerLastIterationNumbers(address _candidate, address _signer) external view returns (uint) {
		return candidateStates[_candidate].signerLastIterationNumbers[_signer];
	}
	/*
	function getMethodId() public returns (bytes4 methodId, bytes4 expectedMethodId) {

		assembly {
			methodId := calldataload(0)
		}

		expectedMethodId = bytes4(keccak256("getMethodId()"));
	}
	*/

    function() external payable {

		bytes4 methodId;

		assembly {
			methodId := calldataload(0)
		}

		uint actionFlowId = methodActionFlowIds[methodId];

		if(!processCall(actionFlowId)) {

			return;
		}

        address _impl = targetContractAddress;

		assembly {
            // Copy the data sent to the memory address starting 0x40
            let ptr := mload(0x40)
            calldatacopy(ptr, 0, calldatasize)

            // Proxy the call to the contract address with the provided gas and data
			let result := call(gas, _impl, callvalue, ptr, calldatasize, 0, 0)

            // Copy the data returned by the proxied call to memory
            let size := returndatasize
            returndatacopy(ptr, 0, size)

            // Check what the result is, return and revert accordingly
			switch result
            case 0 { revert(ptr, size) }
            case 1 { return(ptr, size) }
        }
	}

	// =========================================

	function setCandidateState(address _candidate, bool _isSigner) internal {

		CandidateState storage state = candidateStates[_candidate];

		if(state.iterationNumber == 0)
			state.iterationNumber = 1;

		bool addingSignerStage = (state.iterationNumber % ITERATION_STAGES) == ITERATION_STAGE_ADDING_USER;

		if(_isSigner != addingSignerStage) {

			// If the user has already confirmed removing/adding the new user
			if(state.signerLastIterationNumbers[msg.sender] == state.iterationNumber) {

				state.signerLastIterationNumbers[msg.sender] = 0;
				state.confirmations--;
			}

			return;
		}

		// If the user has already confirmed adding/removing the new user
		if(state.signerLastIterationNumbers[msg.sender] == state.iterationNumber)
			return;

		state.signerLastIterationNumbers[msg.sender] = state.iterationNumber;
		state.confirmations = state.confirmations.add(1);

		uint confirmationLevel = actionFlowStates[ACTION_FLOW_ADMIN].confirmationLevel;

		if(state.confirmations < confirmationLevel)
			return;

		state.iterationNumber++;
		state.confirmations = 0;

		if(_isSigner)
			signerSet.add(_candidate);
		else {
			signerSet.remove(_candidate);
			require(signerSet.elements.length > 0);

			if(confirmationLevel > signerSet.elements.length) {

				actionFlowStates[ACTION_FLOW_ADMIN].confirmationLevel = signerSet.elements.length;
			}
		}
	}

	function processCall(uint _actionFlowId) internal returns (bool) {

		require(signerSet.contains(msg.sender));

		ActionFlowState storage state = actionFlowStates[_actionFlowId];

		bytes32 callHash = keccak256(msg.data);

		require(callHash != bytes32(0));

		if(state.currentCallHash != callHash) {

			state.currentCallHash = callHash;
			state.callNumber = state.callNumber.add(1);
			state.callConfirmations = 0;
		}

		//if(state.callConfirmations >= state.confirmationLevel)
		//	return;

		if(state.signerLastCallNumbers[msg.sender] == state.callNumber)
			return false;

		state.signerLastCallNumbers[msg.sender] = state.callNumber;
		state.callConfirmations = state.callConfirmations.add(1);

		if(state.callConfirmations < state.confirmationLevel)
			return false;

		state.currentCallHash = bytes32(0);
		return true;
	}

	/*
	bytes4 methodId;

	for(uint i = 0; i < 4; i++)
		methodId |= bytes4(msg.data[i] & 0xFF) << (i * 8);
	*/

	function convertBytesToBytes4(bytes memory inBytes) internal returns (bytes4 outBytes4) {

	    if (inBytes.length == 0) {
	        return 0x0;
	    }

	    assembly {
	        outBytes4 := mload(add(inBytes, 4))
	    }
	}
}
