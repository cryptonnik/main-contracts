pragma solidity ^0.5.0;

import "../common/ManagableBaseContract.sol";
import "./OpenZeppelinToken.sol";
import "../openzeppelin/SafeMath.sol";
import "../utils/UintHashSet.sol";

/**
 * @title Standard ERC20 token
 *
 * @dev Implementation of the basic standard token.
 * https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md
 * Originally based on code by FirstBlood: https://github.com/Firstbloodio/token/blob/master/smart_contract/FirstBloodToken.sol
 */
contract FreezableToken is ManagableBaseContract, OpenZeppelinToken {

	event Freeze(
		uint indexed freezeId,
		address indexed account,
		uint amount,
		uint expirationTime
	);

	event FreezeExpired(
		uint indexed freezeId
	);

	event FreezeProlonged(
		uint indexed freezeId,
		uint expirationTime
	);

	event FreezeMakerChanged(
		address freezeMaker
	);

	event IssueMakerChanged(
		address issueMaker
	);

	struct TokenFreeze {

		bool pending;
		address account;
		uint amount;			// Number of tokens
		uint expirationTime;	// timestamp (secs)
	}

	struct FrozenBalanceState {

		uint totalFrozenBalance;
		UintHashSet.Data freezes;
		uint nearestExpirationTime;
	}

	using SafeMath for uint256;
  	using UintHashSet for UintHashSet.Data;

	mapping (address => FrozenBalanceState) _frozenBalanceStates;
	uint _freezeIdCounter;
	mapping (uint => TokenFreeze) _freezes;

	address _freezeMaker;
	address _issueMaker;

	constructor() public {
		performStateUpgrade();
	}

	function performStateUpgrade() internal {
		admin = msg.sender;
		manager = msg.sender;
	}

	function version() public view returns (string memory) {
		return "0.1.0";
	}

	// ===========================================================================

	function balanceOf(address account) public view returns (uint256) {
		return _balances[account].add(_frozenBalanceStates[account].totalFrozenBalance);
	}

	function balanceStateOf(address account) public view
			returns (uint256 freeBalance, uint256 frozenBalance)
	{
		freeBalance = _balances[account];
		frozenBalance = _frozenBalanceStates[account].totalFrozenBalance;
	}

	function balanceExtraStateOf(address account) public view
			returns (
					uint freeBalance,
					uint frozenBalance,
					uint256[] memory freezeAmounts,
					uint256[] memory freezeExpirationTimes)
	{
		FrozenBalanceState storage state = _frozenBalanceStates[account];

		freeBalance = _balances[account];
		frozenBalance = state.totalFrozenBalance;

		freezeAmounts = new uint256[](state.freezes.size());
		freezeExpirationTimes = new uint256[](state.freezes.size());

		for(uint i = 0; i < state.freezes.size(); i++) {

			uint freezeId = state.freezes.elements[i];
			TokenFreeze storage freeze = _freezes[freezeId];

			freezeAmounts[i] = freeze.amount;
			freezeExpirationTimes[i] = freeze.expirationTime;
		}
	}

	function balanceStatesOf(address[] memory accounts) public view
			returns (
					uint[] memory freeBalances,
					uint totalFreeTokens,
					uint[] memory frozenBalances,
					uint totalFrozenTokens)
	{
		freeBalances = new uint[](accounts.length);
		frozenBalances = new uint[](accounts.length);
		totalFreeTokens = 0;
		totalFrozenTokens = 0;

		for(uint i = 0; i < accounts.length; i++) {

			uint freeBalance = _balances[accounts[i]];
			uint frozenBalance = _frozenBalanceStates[accounts[i]].totalFrozenBalance;

			freeBalances[i] = freeBalance;
			frozenBalances[i] = frozenBalance;

			totalFreeTokens = totalFreeTokens.add(freeBalance);
			totalFrozenTokens = totalFrozenTokens.add(frozenBalance);
		}
	}

	// ===========================================================================

	function setIssueMaker(address issueMaker) public onlyManager {

		_issueMaker = issueMaker;
		emit IssueMakerChanged(issueMaker);
	}

	function issueMaker() public view returns (address) {
		return _issueMaker;
    }

	modifier onlyIssueMaker() {
		require(msg.sender == _issueMaker);
		_;
	}


	function setFreezeMaker(address freezeMaker) public onlyManager {

		_freezeMaker = freezeMaker;
		emit FreezeMakerChanged(freezeMaker);
	}

	function freezeMaker() public view returns (address) {
		return _freezeMaker;
    }

	modifier onlyFreezeMaker() {
		require(msg.sender == _freezeMaker);
		_;
	}

	// ===========================================================================

    function transfer(address to, uint256 value) public returns (bool) {

		require(to != address(0));

		if(_balances[msg.sender] < value) {

			_checkFreezes(msg.sender);
			require(_balances[msg.sender] >= value);
		}

		_balances[msg.sender] = _balances[msg.sender].sub(value);
		_balances[to] = _balances[to].add(value);
		emit Transfer(msg.sender, to, value);
		return true;
    }

	function transferFrom(address from, address to, uint256 value )
			public returns (bool)
	{
		require(value <= _allowed[from][msg.sender]);
		require(to != address(0));

		if(_balances[msg.sender] < value) {

			_checkFreezes(msg.sender);
			require(_balances[msg.sender] >= value);
		}

		_balances[from] = _balances[from].sub(value);
		_balances[to] = _balances[to].add(value);
		_allowed[from][msg.sender] = _allowed[from][msg.sender].sub(value);
		emit Transfer(from, to, value);
		return true;
	}

	// ===========================================================================

	function issueTokens(address account, uint amount) public onlyIssueMaker {
		_mint(account, amount);
	}

	function issueFrozenTokens(address account, uint amount, uint expirationTime) public onlyIssueMaker {

		require(account != address(0));

		_totalSupply = _totalSupply.add(amount);
	    emit Transfer(address(0), account, amount);

		_createFreeze(account, amount, expirationTime);
	}

	function issueFrozenTokens(
			address[] memory accounts,
			uint[] memory amounts,
			uint[] memory expirationTimes) public onlyIssueMaker
	{
		require(accounts.length == amounts.length);
		require(accounts.length == expirationTimes.length);

		for(uint i = 0; i < accounts.length; i++)
			issueFrozenTokens(accounts[i], amounts[i], expirationTimes[i]);
	}

	// ===========================================================================

	function freezeAllTokens(address account, uint expirationTime)
			public onlyFreezeMaker
			returns (uint totalBalance)
	{
		require(account != address(0));

		uint balance = _balances[account];
		FrozenBalanceState storage frozenBalanceState = _frozenBalanceStates[account];

		totalBalance = balance.add(frozenBalanceState.totalFrozenBalance);

		if(balance == 0)
			return totalBalance;

		if(frozenBalanceState.totalFrozenBalance > 0 &&
			frozenBalanceState.nearestExpirationTime < expirationTime)
		{
			uint[] storage freezeIds = frozenBalanceState.freezes.elements;

			for(uint i = 0; i < freezeIds.length; i++) {

				uint freezeId = freezeIds[i];

				TokenFreeze storage freeze = _freezes[freezeId];

				if(freeze.expirationTime < expirationTime) {

					freeze.expirationTime = expirationTime;
					emit FreezeProlonged(freezeId, expirationTime);
				}
			}

			frozenBalanceState.nearestExpirationTime = expirationTime;
		}

		_createFreeze(account, balance, expirationTime);
	}

	function forceFreezeCheck(address account) public returns (uint) {
		_checkFreezes(account);
	}

	function forceFreezeCheck(address[] memory accounts) public returns (uint) {

		for(uint i = 0; i < accounts.length; i++)
			_checkFreezes(accounts[i]);
	}

	// ===========================================================================

	function _checkFreezes(address account) internal returns (uint) {

		FrozenBalanceState storage frozenBalanceState = _frozenBalanceStates[account];

		if(frozenBalanceState.totalFrozenBalance == 0)
			return 0;

		if(frozenBalanceState.nearestExpirationTime > block.timestamp)
			return 0;

		uint[] memory freezeIds = frozenBalanceState.freezes.elements;

		uint nearestExpirationTime;

		uint totalUnfrozen = 0;

		for(uint i = 0; i < freezeIds.length; i++) {

			uint freezeId = freezeIds[i];

			TokenFreeze storage freeze = _freezes[freezeId];

			if(freeze.expirationTime <= block.timestamp) {

				// Let's unfreeze some tokens

				freeze.pending = false;
				frozenBalanceState.freezes.remove(freezeId);

				frozenBalanceState.totalFrozenBalance =
						frozenBalanceState.totalFrozenBalance.sub(freeze.amount);

				_balances[account] = _balances[account].add(freeze.amount);
				totalUnfrozen = totalUnfrozen.add(freeze.amount);

				emit FreezeExpired(freezeId);

			} else {
				if(i == 0 || freeze.expirationTime < nearestExpirationTime)
					nearestExpirationTime = freeze.expirationTime;
			}
		}

		frozenBalanceState.nearestExpirationTime = nearestExpirationTime;
		return totalUnfrozen;
	}

	function _createFreeze(address account, uint amount, uint expirationTime) internal {

		_freezeIdCounter = _freezeIdCounter.add(1);

		uint freezeId = _freezeIdCounter;

		_freezes[freezeId] = TokenFreeze({ pending: true, account: account, amount: amount, expirationTime: expirationTime });

		FrozenBalanceState storage frozenBalanceState = _frozenBalanceStates[account];

		frozenBalanceState.totalFrozenBalance = frozenBalanceState.totalFrozenBalance.add(amount);
		frozenBalanceState.freezes.add(freezeId);

		if(frozenBalanceState.nearestExpirationTime >= expirationTime)
			frozenBalanceState.nearestExpirationTime = expirationTime;

		emit Freeze(freezeId, account, amount, expirationTime);
	}
}
