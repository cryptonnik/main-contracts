pragma solidity ^0.5.0;

import "../common/ManagableBaseContract.sol";
import "../openzeppelin/ERC20.sol";
import "../utils/AddressHashSet.sol";


contract Token is ManagableBaseContract, ERC20 {

	string public name = 'IMMO';
	string public symbol = 'IMMO';
	uint public decimals = 6;

	constructor() public {
		performStateUpgrade();
	}

	function performStateUpgrade() internal {
		admin = msg.sender;
		manager = msg.sender;
	}

	function version() public view returns (string memory) {
		return "0.1.0";
	}

	function issue(address account, uint256 amount) public onlyManager {

		_mint(account, amount);
		require(totalSupply() <= 300 * 1000000);
	}

	function redeem(address account, uint256 amount) public onlyManager {
		_burn(account, amount);
	}


}
