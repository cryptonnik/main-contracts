pragma solidity ^0.5.0;

import "../common/ManagableBaseContract.sol";

interface IERC20 {
    function balanceOf(address who) external view returns (uint256);
}

contract TrustStorage is ManagableBaseContract {

	mapping (bytes32 => int) intValues;
	mapping (bytes32 => uint) uintValues;

	mapping (bytes32 => address) addressValues;
	mapping (bytes32 => bool) booleanValues;

	mapping (bytes32 => string) stringValues;
	mapping (bytes32 => bytes) bytesValues;

	constructor() public {
		performStateUpgrade();
	}

	function performStateUpgrade() internal {
		admin = msg.sender;
		manager = msg.sender;
	}

	function version() public view returns (string memory) {
		return "0.1.0";
	}

	// ====================================================================

    function getIntProperty(bytes32 key) public view returns (int) {
        return intValues[key];
    }

    function getUintProperty(bytes32 key) public view returns (uint) {
        return uintValues[key];
    }

    function getAddressProperty(bytes32 key) public view returns (address) {
        return addressValues[key];
    }

    function getBooleanProperty(bytes32 key) public view returns (bool) {
        return booleanValues[key];
    }

    function getStringProperty(bytes32 key) public view returns (string memory) {
        return stringValues[key];
    }

    function getBytesProperty(bytes32 key) public view returns (bytes memory) {
        return bytesValues[key];
    }

	// ====================================================================

    function setIntProperty(bytes32 key, int value) onlyManager public {
        intValues[key] = value;
    }

    function setUintProperty(bytes32 key, uint value) onlyManager public {
        uintValues[key] = value;
    }

    function setAddressProperty(bytes32 key, address value) onlyManager public {
        addressValues[key] = value;
    }

    function setBooleanProperty(bytes32 key, bool value) onlyManager public {
        booleanValues[key] = value;
    }

    function setStringProperty(bytes32 key, string memory value) onlyManager public {
        stringValues[key] = value;
    }

    function setBytesProperty(bytes32 key, bytes memory value) onlyManager public {
        bytesValues[key] = value;
    }

	// ====================================================================

    function getIntProperty(string memory key) public view returns (int) {
        return getIntProperty(keccak256(abi.encodePacked(key)));
    }

    function getUintProperty(string memory key) public view returns (uint) {
        return getUintProperty(keccak256(abi.encodePacked(key)));
    }

    function getAddressProperty(string memory key) public view returns (address) {
        return getAddressProperty(keccak256(abi.encodePacked(key)));
    }

    function getBooleanProperty(string memory key) public view returns (bool) {
        return getBooleanProperty(keccak256(abi.encodePacked(key)));
    }

    function getStringProperty(string memory key) public view returns (string memory) {
        return getStringProperty(keccak256(abi.encodePacked(key)));
    }

    function getBytesProperty(string memory key) public view returns (bytes memory) {
        return getBytesProperty(keccak256(abi.encodePacked(key)));
    }

	// ====================================================================

    function setIntProperty(string calldata key, int value) external {
        setIntProperty(keccak256(abi.encodePacked(key)), value);
    }

    function setUintProperty(string calldata key, uint value) external {
        setUintProperty(keccak256(abi.encodePacked(key)), value);
    }

    function setAddressProperty(string calldata key, address value) external {
        setAddressProperty(keccak256(abi.encodePacked(key)), value);
    }

    function setBooleanProperty(string calldata key, bool value) external {
        setBooleanProperty(keccak256(abi.encodePacked(key)), value);
    }

    function setStringProperty(string calldata key, string calldata value) external {
        setStringProperty(keccak256(abi.encodePacked(key)), value);
    }

    function setBytesProperty(string calldata key, bytes calldata value) external {
        setBytesProperty(keccak256(abi.encodePacked(key)), value);
    }

	// ====================================================================

	function getTokenType(string memory tokenName) public view returns (string memory) {

        bytes32 key = keccak256(abi.encodePacked("token:", tokenName, ":type"));
        return getStringProperty(key);
	}

    function getErc20TokenAddress(string memory tokenName) public view returns (address) {

        bytes32 key = keccak256(abi.encodePacked("token:", tokenName, ":address"));
        return getAddressProperty(key);
    }

    function getErc20TokenAccountBalance(string memory tokenName, address account) public view returns (uint256) {

        string memory tokenType = getTokenType(tokenName);
        require(keccak256(abi.encodePacked(tokenType)) == keccak256(abi.encodePacked("Ethereum-ERC20")));

        address tokenAddress = getErc20TokenAddress(tokenName);
        require(tokenAddress != address(0));

        IERC20 erc20 = IERC20(tokenAddress);
        return erc20.balanceOf(account);
    }

}
