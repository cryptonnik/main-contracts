pragma solidity ^0.5.0;

contract StorageGasCostsExperiment {

    struct FrozenBalanceState {

		uint totalFrozenBalance;
		uint nearestExpirateTime;
	}

	mapping (address => FrozenBalanceState) _frozenBalanceStates;

	function freeze(address account, uint amount) public {

	    FrozenBalanceState storage state = _frozenBalanceStates[account];

	    state.totalFrozenBalance = state.totalFrozenBalance + amount;
	}

	function freeze2(address account, uint amount) public {

	    FrozenBalanceState storage state = _frozenBalanceStates[account];

	    state.totalFrozenBalance = state.totalFrozenBalance + amount;
	    state.nearestExpirateTime = block.timestamp;
	}

	function readStorageMultipleTimes(address account, uint amount, uint checkNumber) public {

	    FrozenBalanceState storage state = _frozenBalanceStates[account];

	    bool updateExpirationDate = true;

	    for(uint i = 0; i < checkNumber; i++) {

	        if(state.nearestExpirateTime > block.timestamp)
	            updateExpirationDate = false;
	    }

	    state.totalFrozenBalance = state.totalFrozenBalance + amount;

	    if(updateExpirationDate || true)
	        state.nearestExpirateTime = block.timestamp;
	}

	function readStorageWithCache(address account, uint amount, uint checkNumber) public {

	    FrozenBalanceState storage state = _frozenBalanceStates[account];

	    bool updateExpirationDate = true;
	    uint nearestExpirateTime = state.nearestExpirateTime;

	    for(uint i = 0; i < checkNumber; i++) {

	        if(nearestExpirateTime > block.timestamp)
	            updateExpirationDate = false;
	    }

	    state.totalFrozenBalance = state.totalFrozenBalance + amount;

	    if(updateExpirationDate || true)
	        state.nearestExpirateTime = block.timestamp;
	}
}
