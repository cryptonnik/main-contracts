pragma solidity ^0.5.0;

import "../common/ManagableBaseContract.sol";
import "../utils/Bytes32HashSet.sol";

contract High1000 is ManagableBaseContract {

	using Bytes32HashSet for Bytes32HashSet.Data;

    struct PropertySet {

        mapping (uint => uint) uints;
        mapping (uint => int) ints;
        mapping (uint => bool) booleans;
        mapping (uint => address) addresses;
        mapping (uint => string) strings;
        mapping (uint => bytes) byteArrays;
    }

	event MemberAdded(
			bytes32 memberId,
			string name,
			uint8 groupId,
			uint registrationTime,
			string country,
			string linkedInProfile);

	event MemberStringPropertyChanged(
			bytes32 memberId,
			uint propertyIndex,
			string newValue);

	event MemberUintPropertyChanged(
			bytes32 memberId,
			uint propertyIndex,
			uint newValue);

	event MemberRemoved(bytes32 memberId);

	uint constant PROPERTY_STRING_NAME = 1;
	uint constant PROPERTY_STRING_COUNTRY = 2;
	uint constant PROPERTY_STRING_LINKEDIN = 3;
	uint constant PROPERTY_UINT_GROUP_ID = 1001;
	uint constant PROPERTY_UINT_REGISTRATION_TIME = 1002;

	uint constant GROUP_ID_EVANGELIST = 1;
	uint constant GROUP_ID_1_100 = 2;
	uint constant GROUP_ID_101_300 = 3;
	uint constant GROUP_ID_301_1000 = 4;

	Bytes32HashSet.Data memberSet;
	mapping(bytes32 => PropertySet) membersProperties;

	constructor() public {
		performStateUpgrade();
	}

	function performStateUpgrade() internal {
		admin = msg.sender;
		manager = msg.sender;
	}

	function version() public view returns (string memory) {
		return "0.2.0";
	}

	// ====================================

	function addMember(
			string memory _name,
			uint8 _groupId,
			uint _registrationTime,
			string memory _country,
			string memory _linkedInProfile) public onlyManager returns (bytes32) {

		bytes32 memberId = keccak256(abi.encodePacked(_name, _country, block.timestamp));

		require(!memberSet.contains(memberId));

		membersProperties[memberId].strings[PROPERTY_STRING_NAME] = _name;
		membersProperties[memberId].uints[PROPERTY_UINT_GROUP_ID] = _groupId;
		membersProperties[memberId].uints[PROPERTY_UINT_REGISTRATION_TIME] = _registrationTime;
		membersProperties[memberId].strings[PROPERTY_STRING_COUNTRY] = _country;
		membersProperties[memberId].strings[PROPERTY_STRING_LINKEDIN] = _linkedInProfile;

		memberSet.add(memberId);

		emit MemberAdded(
				memberId,
				_name,
				_groupId,
				_registrationTime,
				_country,
				_linkedInProfile);

		return memberId;
	}

	function setMemberName(bytes32 _memberId, string memory _name) public onlyManager {
		setStringProperty(_memberId, PROPERTY_STRING_NAME, _name);
	}

	function setMemberCountry(bytes32 _memberId, string memory _country) public onlyManager {
		setStringProperty(_memberId, PROPERTY_STRING_COUNTRY, _country);
	}

	function setMemberLinkedInProfile(bytes32 _memberId, string memory _linkedInProfile) public onlyManager {
		setStringProperty(_memberId, PROPERTY_STRING_LINKEDIN, _linkedInProfile);
	}

	function setMemberGroup(bytes32 _memberId, uint8 _groupId) public onlyManager {
		setUintProperty(_memberId, PROPERTY_UINT_GROUP_ID, _groupId);
	}

	function setMemberRegistrationTime(bytes32 _memberId, uint _registrationTime) public onlyManager {
		setUintProperty(_memberId, PROPERTY_UINT_REGISTRATION_TIME, _registrationTime);
	}

	function deleteMember(bytes32 _memberId) public onlyManager {

		require(memberSet.contains(_memberId));

		for(uint i = PROPERTY_STRING_NAME; i <= PROPERTY_STRING_LINKEDIN; i++)
			delete membersProperties[_memberId].strings[i];

		for(uint i = PROPERTY_UINT_GROUP_ID; i <= PROPERTY_UINT_REGISTRATION_TIME; i++)
			delete membersProperties[_memberId].uints[i];

		memberSet.remove(_memberId);
		emit MemberRemoved(_memberId);
	}

	function memberIds() public view returns (bytes32[] memory numbers) {
		return memberSet.elements;
	}

	function memberGroupOf(bytes32 _memberId) public view returns (uint) {
		return membersProperties[_memberId].uints[PROPERTY_UINT_GROUP_ID];
	}

	function memberInfo(bytes32 _memberId) public view
			returns (
					string memory name,
					uint groupId,
					uint registrationTime,
					string memory country,
					string memory linkedInProfile)
	{
		name = membersProperties[_memberId].strings[PROPERTY_STRING_NAME];
		groupId = membersProperties[_memberId].uints[PROPERTY_UINT_GROUP_ID];
		registrationTime = membersProperties[_memberId].uints[PROPERTY_UINT_REGISTRATION_TIME];

		country = membersProperties[_memberId].strings[PROPERTY_STRING_COUNTRY];
		linkedInProfile = membersProperties[_memberId].strings[PROPERTY_STRING_LINKEDIN];
	}

	// =========================================

	function setStringProperty(bytes32 _memberId, uint propertyIndex, string memory _value) internal {

		require(memberSet.contains(_memberId));
		membersProperties[_memberId].strings[propertyIndex] = _value;
		emit MemberStringPropertyChanged(_memberId, propertyIndex, _value);
	}

	function setUintProperty(bytes32 _memberId, uint propertyIndex, uint _value) internal {

		require(memberSet.contains(_memberId));
		membersProperties[_memberId].uints[propertyIndex] = _value;
		emit MemberUintPropertyChanged(_memberId, propertyIndex, _value);
	}
}
