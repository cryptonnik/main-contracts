pragma solidity ^0.5.0;

import "../../contracts/trust/TrustStorage.sol";

contract TrustStorageV2 is TrustStorage {

	uint public versionCode;

	function performStateUpgrade() internal {
		versionCode = 2;
	}

	function version() public view returns (string memory) {
		return "0.1.1";
	}
}
