
const Proxy = artifacts.require("Proxy");
const Contract = artifacts.require("TrustStorage");

contract('ManagableUpdatableTrustStorage', async (accounts) => {
	
	let contract = null;
	
	it("should deploy the contract", async () => {
				
		var firstVersionCode = await Contract.new();
		var proxy = await Proxy.new(firstVersionCode.address);
		
		contract = await Contract.at(proxy.address);
		
		assert.equal(await contract.currentImplementation(), firstVersionCode.address);
		assert.equal(await contract.admin(), accounts[0]);
		assert.equal(await contract.manager(), accounts[0]);
	});
		
	it("should change a manager", async () => {
		
		await contract.setManager(accounts[1]);
		assert.equal(await contract.admin(), accounts[0]);
		assert.equal(await contract.manager(), accounts[1]);
	});	
	
	it("should fail changing a manager because of not owner", async () => {
		
		await expectException(
				contract.setManager(accounts[2], { from: accounts[1] }));
		
		await expectException(
				contract.setManager(accounts[3], { from: accounts[2] }));
	});
});

async function expectException(f) {
	
	var exceptionOccurred = true;
	
	try {
		await f;
		exceptionOccurred = false;
		
	} catch(ex) { }
	
	assert.isOk(exceptionOccurred, "Exceptions not occurred");
}