
const Proxy = artifacts.require("Proxy");
const TrustStorage = artifacts.require("TrustStorage");

contract('ReleasedTrustStorage', async (accounts) => {

	let contract = null;

	it("should deploy the contract", async () => {

		var firstVersionCode = await TrustStorage.new();
		var proxy = await Proxy.new(firstVersionCode.address);

		contract = await TrustStorage.at(proxy.address);

		assert.equal(await contract.currentImplementation(), firstVersionCode.address);
		assert.equal(await contract.admin(), accounts[0]);
		assert.equal(await contract.manager(), accounts[0]);
	});

	it("should change the manager", async () => {

		await contract.setManager(accounts[1], { from: accounts[0] });
		assert.equal(await contract.manager(), accounts[1]);
	});

	const INT_KEY = "int.property";
	const UINT_KEY = "uint.property";
	const ADDRESS_KEY = "address.property";
	const BOOLEAN_KEY = "boolean.property";
	const STRING_KEY = "string.property";
	const BYTES_KEY = "bytes.property";

	const INT_VALUE = -24324234;
	const UINT_VALUE = 94783453453;
	const ADDRESS_VALUE = "0x692a70D2e424a56D2C6C27aA97D1a86395877b3A";
	const BOOLEAN_VALUE = true;
	const STRING_VALUE = "Hello, World!";
	const BYTES_VALUE = "0x023424db234344234a093423423be234234243b009596453ff2344ff23424bc00056348222";

	it("should update member properties", async () => {

		await contract.methods["setIntProperty(string,int256)"](INT_KEY, INT_VALUE, { from: accounts[1] });
		await contract.methods["setUintProperty(string,uint256)"](UINT_KEY, UINT_VALUE, { from: accounts[1] });
		await contract.methods["setAddressProperty(string,uint256)"](ADDRESS_KEY, ADDRESS_VALUE, { from: accounts[1] });
		await contract.methods["setBooleanProperty(string,boolean)"](BOOLEAN_KEY, BOOLEAN_VALUE, { from: accounts[1] });
		await contract.methods["setStringProperty(string,string)"](STRING_KEY, STRING_VALUE, { from: accounts[1] });
		await contract.methods["setBytesProperty(string,bytes)"](BYTES_KEY, BYTES_VALUE, { from: accounts[1] });

		assert.equal(await contract.methods["getIntProperty(string)"](INT_KEY), INT_VALUE);
		assert.equal(await contract.methods["getUintProperty(string)"](UINT_KEY), UINT_VALUE);
		assert.equal(await contract.methods["getAddressProperty(string)"](ADDRESS_KEY), ADDRESS_VALUE);
		assert.equal(await contract.methods["getBooleanProperty(string)"](BOOLEAN_KEY), BOOLEAN_VALUE);
		assert.equal(await contract.methods["getStringProperty(string)"](STRING_KEY), STRING_VALUE);
		assert.equal(await contract.methods["getBytesProperty(string)"](BYTES_KEY), BYTES_VALUE);
	});

	it("should fail setting values because the contract is being edited by not a manager", async () => {

		await expectException(
				contract.methods["setIntProperty(string,int256)"](
						INT_KEY, INT_VALUE, { from: accounts[0] }));

		await expectException(
				contract.methods["setUintProperty(string,int256)"](
						UINT_KEY, UINT_VALUE, { from: accounts[0] }));

		await expectException(
				contract.methods["setAddressProperty(string,uint256)"](
						ADDRESS_KEY, ADDRESS_VALUE, { from: accounts[0] }));

		await expectException(
				contract.methods["setBooleanProperty(string,boolean)"](
						BOOLEAN_KEY, BOOLEAN_VALUE, { from: accounts[0] }));

		await expectException(
				contract.methods["setStringProperty(string,string)"](
						STRING_KEY, STRING_VALUE, { from: accounts[0] }));

		await expectException(
				contract.methods["setBytesProperty(string,bytes)"(
						BYTES_KEY, BYTES_VALUE, { from: accounts[0] }));
	});

	it("should throw an exception if some ethers sent", async () => {

		var balance = parseInt(await web3.eth.getBalance(accounts[0]));
		var weisToSend = 100; //web3.toWei(1, "ether");

		//console.log("Balance: " + balance);
		//console.log("Target: " + weisToSend);

		assert.isAtLeast(balance, weisToSend, "Not enough ethers");

		await expectException(
				contract.send(weisToSend));

		//console.log("Balance: " + web3.eth.getBalance(contract.address).toNumber());
	});
});

function compareArrays(array1, array2) {

	assert.equal(array1.length, array2.length);

	for(var i = 0; i < array1.length; i++) {

		assert.equal(array1[i], array2[i]);
	}
}

async function expectException(f) {

	var exceptionOccurred = true;

	try {
		await f;
		exceptionOccurred = false;

	} catch(ex) { }

	assert.isOk(exceptionOccurred, "Exceptions not occurred");
}
