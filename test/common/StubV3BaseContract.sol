pragma solidity ^0.5.0;

import "../../contracts/common/BaseContract.sol";

contract StubV3BaseContract is BaseContract {

	uint public versionCode;

	function performStateUpgrade() internal {

		versionCode = 3;
	}

	function version() public view returns (string memory) {
		return "1.1.0";
	}
}
