const Contract = artifacts.require("StubManagableBaseContract");

contract('ManagableBaseContract', async (accounts) => {
	
	let contract = null;
	
	it("should deploy the contract", async () => {
		
		contract = await Contract.new();
		assert.equal(await contract.admin(), accounts[0]);
		assert.equal(await contract.manager(), accounts[0]);
	});
	
	it("should change a manager", async () => {
		
		await contract.setManager(accounts[1]);
		assert.equal(await contract.admin(), accounts[0]);
		assert.equal(await contract.manager(), accounts[1]);
	});	
	
	it("should fail changing a manager because of not owner", async () => {
		
		await expectException(
				contract.setManager(accounts[2], { from: accounts[1] }));
		
		await expectException(
				contract.setManager(accounts[3], { from: accounts[2] }));
	});
	
	it("should call the only manager method", async () => {
				
		await contract.doAsManager({ from: accounts[1] });
	});
	
	it("should fail the only manager method call", async () => {
		
		await expectException(
			contract.doAsManager({ from: accounts[0] }));
		
		await expectException(
			contract.doAsManager({ from: accounts[2] }));
	});
});

async function expectException(f) {
	
	var exceptionOccurred = true;
	
	try {
		await f;
		exceptionOccurred = false;
		
	} catch(ex) { }
	
	assert.isOk(exceptionOccurred, "Exceptions not occurred");
}