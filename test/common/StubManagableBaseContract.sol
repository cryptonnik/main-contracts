pragma solidity ^0.5.0;

import "../../contracts/common/ManagableBaseContract.sol";

contract StubManagableBaseContract is ManagableBaseContract {

	constructor() public {
		performStateUpgrade();
	}

	function performStateUpgrade() internal {
		admin = msg.sender;
		manager = msg.sender;
	}

	function version() public view returns (string memory) {
		return "1.0.0";
	}

	function doAsManager() public onlyManager { }
}
