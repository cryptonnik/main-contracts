pragma solidity ^0.5.0;

import "../../contracts/common/BaseContract.sol";

contract StubV2BaseContract is BaseContract {

	uint public versionCode;

	function performStateUpgrade() internal {

		versionCode = 2;
	}

	function version() public view returns (string memory) {
		return "1.0.1";
	}
}
