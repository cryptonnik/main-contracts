
const FreezableToken = artifacts.require("FreezableToken");

contract('FreezableToken', async (accounts) => {

	/*
	it("should issue frozen tokens", async () => {

		const amountToIssue = 100;
		const amountToTransfer = 10;
		const timeToUnfreeze = 5;
		const timeToSleep = 6000;

		var contract = await FreezableToken.new();
		await contract.issueFrozenTokens(accounts[0], amountToIssue, (Date.now() / 1000 | 0) + timeToUnfreeze);

		assert.equal(await contract.balanceOf(accounts[0]), amountToIssue);
		assert.equal(await contract.freeBalanceOf(accounts[0]), 0);
		assert.equal(await contract.frozenBalanceOf(accounts[0]), amountToIssue);

		await sleep(timeToSleep);

		await contract.transfer(accounts[1], amountToTransfer, { from: accounts[0] });

		assert.equal(await contract.balanceOf(accounts[0]), amountToIssue - amountToTransfer);
		assert.equal(await contract.freeBalanceOf(accounts[0]), amountToIssue - amountToTransfer);
		assert.equal(await contract.frozenBalanceOf(accounts[0]), 0);

		assert.equal(await contract.balanceOf(accounts[1]), amountToTransfer);
		assert.equal(await contract.freeBalanceOf(accounts[1]), amountToTransfer);
		assert.equal(await contract.frozenBalanceOf(accounts[1]), 0);
	});
	*/
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function compareArrays(array1, array2) {

	assert.equal(array1.length, array2.length);

	for(var i = 0; i < array1.length; i++) {

		assert.equal(array1[i], array2[i]);
	}
}

async function expectException(f) {

	var exceptionOccurred = true;

	try {
		await f;
		exceptionOccurred = false;

	} catch(ex) { }

	assert.isOk(exceptionOccurred, "Exceptions not occurred");
}
