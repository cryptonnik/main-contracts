
const MultisigContractCaller = artifacts.require("MultisigContractCaller");

contract('MultisigContractCaller', async (accounts) => {
	
	let contract = null;
	
	it("should deploy the contract", async () => {
		
		contract = await MultisigContractCaller.new();
		/*
		var result = await contract.getMethodId.call();		
		console.log(result[0]);
		console.log(result[1]);
		*/
	});
	
});

function compareArrays(array1, array2) {
	
	assert.equal(array1.length, array2.length);
	
	for(var i = 0; i < array1.length; i++) {
		
		assert.equal(array1[i], array2[i]);
	}
}

async function expectException(f) {
	
	var exceptionOccurred = true;
	
	try {
		await f;
		exceptionOccurred = false;
		
	} catch(ex) { }
	
	assert.isOk(exceptionOccurred, "Exceptions not occurred");	
}
