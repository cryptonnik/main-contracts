pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "../../contracts/utils/Bytes32HashSet.sol";

contract TestBytes32HashSet {

	using Bytes32HashSet for Bytes32HashSet.Data;

	/*
    function testSetModifications() {

        HashAddressSet.Data set;

        set.add(1);
        Assert.equal(set.contains(1), true, "The set must contain the added element");
        Assert.equal(set.elements.length, 1, "The set must have a correct length");

        set.remove(1);
        Assert.equal(set.contains(1), false, "The set must not contain the removed element");
        Assert.equal(set.elements.length, 0, "The set must have a correct length");

        set.add(1);
        Assert.equal(set.contains(1), true, "The set must contain the added element");
        Assert.equal(set.elements.length, 1, "The set must have a correct length");

        set.add(2);
        set.add(3);
        Assert.equal(set.contains(2), true, "The set must contain the added element");
        Assert.equal(set.contains(3), true, "The set must contain the added element");
        Assert.equal(set.elements.length, 3, "The set must have a correct length");

        set.remove(3);
        Assert.equal(set.contains(3), false, "The set must not contain the removed element");
        Assert.equal(set.elements.length, 2, "The set must have a correct length");

        set.add(2);
        Assert.equal(set.contains(2), true, "The set must contain the added element");
        Assert.equal(set.elements.length, 2, "The set must have a correct length");

        set.add(4);
        Assert.equal(set.contains(4), true, "The set must contain the added element");
        Assert.equal(set.elements.length, 3, "The set must have a correct length");

        set.remove(2);
        Assert.equal(set.contains(2), false, "The set must not contain the removed element");
        Assert.equal(set.elements.length, 2, "The set must have a correct length");

        set.remove(5);
        Assert.equal(set.elements.length, 2, "The set must have a correct length");

        set.remove(4);
        Assert.equal(set.contains(4), false, "The set must not contain the removed element");
        Assert.equal(set.elements.length, 1, "The set must have a correct length");
    }
	*/
}
